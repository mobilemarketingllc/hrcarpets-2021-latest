<?php
global $post;
$flooringtype = $post->post_type; 
$brand = get_field('brand') ;
$collection = get_field('collection');
$image = get_field('swatch_image_link') ? get_field('swatch_image_link'):"http://placehold.it/168x123?text=No+Image"; 
	
	if(strpos($image , 's7.shawimg.com') !== false){
		if(strpos($image , 'http') === false){ 
		$image = "http://" . $image;
	}	
		$image = "https://mobilem.liquifire.com/mobilem?source=url[".$image . "]&scale=size[300x300]&sink";
	}else{
		if(strpos($image , 'http') === false){ 
		$image = "https://" . $image;
	}	
		$image= "https://mobilem.liquifire.com/mobilem?source=url[".$image . "]&scale=size[600x400]&sink";
	}

	$room_image_small = array();
	if(get_field('gallery_room_images')){
		$gallery_images = get_field('gallery_room_images');
		$gallery_img = explode("|",$gallery_images);
		
		foreach($gallery_img as  $key=>$value) {
			$room_image = $value;
		
			if(strpos($room_image , 's7.shawimg.com') !== false){
				if(strpos($room_image , 'http') === false){ 
					$room_image = "http://" . $room_image;
				}
				$room_image_small[] = $room_image ;
				$room_image = $room_image ;
			} else{
				if(strpos($room_image , 'http') === false){ 
					$room_image = "https://" . $room_image;
				}
				$room_image_small[]= "https://mobilem.liquifire.com/mobilem?source=url[".$room_image . "]&scale=size[150]&sink";
				$room_image= "https://mobilem.liquifire.com/mobilem?source=url[".$room_image . "]&scale=size[600x400]&sink";
			}
		}
    }
  
?>
	<article <?php post_class( 'fl-post' ); ?> id="fl-post-<?php the_ID(); ?>" >

		<header class="fl-post-header">

			<?php //FLTheme::post_top_meta(); ?>
		</header><!-- .fl-post-header -->

		<div class="fl-post-content clearfix grey-back" itemprop="text">

			<div class="container">
				<div class="row">
					<div class="col-md-8 col-sm-8 product-swatch  <?php if($collection == 'COREtec Colorwall' || $collection == 'Coretec Colorwall') { echo 'colorwall_product_swatch'; }?>">
					
						<div class="img-responsive toggle-image" style="background-image:url('<?php echo $image; ?>');background-size: cover;">
							<img src="<?php echo $image; ?>" class="img-responsive toggle-image" />
						</div>

						<?php

						if(get_field('gallery_room_images')){ ?>
						<div class="toggle-image-thumbnails">
						<a href="#" data-background="<?php echo $image ?>" data-fr-replace-bg=".toggle-image" style="background-image:url('<?php echo $image ?>');background-size: cover;"></a>
							<?php
							foreach($room_image_small as $k=>$v){
							?><a href="#" data-background="<?php echo $v ?>" data-fr-replace-bg=".toggle-image" style="background-image:url('<?php echo $v ?>');background-size: cover;"></a><?php
							}
							?>
						</div>
						<?php } ?>




					</div>
					<div class="col-md-4 col-sm-4 product-box">

						<?php get_template_part('includes/product-brand-logos'); ?>

						<?php if(get_field('parent_collection')) { ?>
						<h2><?php the_field('parent_collection'); ?> </h2>
						<?php } ?>


						<h2><?php //the_field('collection'); ?></h2>
						<h1 class="fl-post-title" itemprop="name">
							<?php the_field('collection'); ?>
						</h1>
						<h2 class="fl-post-title" itemprop="name">
							<?php the_field('color'); ?>
						</h2>

						<div class="product-colors">
							<?php
								$value = get_post_meta($post->ID, 'collection', true);
								$key =  "collection";

								$args = array(
									'post_type'      => $flooringtype,
									'posts_per_page' => -1,
									'post_status'    => 'publish',
									'meta_query'     => array(
										array(
											'key'     => $key,
											'value'   => $value,
											'compare' => '='
										)
									)
								);
							?>
							<?php
							$the_query = new WP_Query( $args );
							?>
							<ul>
								<li class="found"><?php  echo $the_query ->found_posts; ?></li>


								<li class="colors"><?php if($the_query ->found_posts > 1) { echo "Colors"; }else{ echo "Color";}?><br/>Available</li>
							</ul>

						</div>

						<a href="<?php echo site_url(); ?>/flooring-coupon/?keyword=<?php echo @$_COOKIE['keyword']; ?>&brand=<?php echo get_field('brand');?>" class="fl-button" role="button" style="width: auto;">
							<span class="fl-button-text">GET COUPON</span>
						</a>
						<br />
						<a href="<?php echo get_page_link(13); ?>" class="fl-button btn-white" role="button" style="width: auto;">
							<span class="fl-button-text">CONTACT US FOR INFO</span>
						</a>
						<br />
						
						<div class="product-atts">

						</div>
					</div>
				</div>


				<?php get_template_part('includes/product-color-slider'); ?>
			</div>



		</div><!-- .fl-post-content -->
		<div class="container">
			<?php get_template_part('includes/product-attributes'); ?>
		</div>

		<?php //FLTheme::post_bottom_meta(); ?>
		<?php //FLTheme::post_navigation(); ?>
		<?php //comments_template(); ?>

	</article>
	<!-- .fl-post -->

	<?php
	$title = get_the_title();
	$sku = get_field('sku', $post->ID);
	$brand = get_field('brand_facet', $post->ID);
	$jsonld = array('@context'=>'https://schema.org/','@type'=>'Product','name'=> $title,'image'=>$image,'description'=>$title,'sku'=>$sku,'mpn'=>$sku,'brand'=>array('@type'=>'thing','name'=>$brand), 
	'offers'=>array('@type'=>'offer','priceCurrency'=>'USD','price'=>'00','priceValidUntil'=>''));
	?>
	<?php echo '<script type="application/ld+json">'.json_encode($jsonld).'</script>';	?>
	