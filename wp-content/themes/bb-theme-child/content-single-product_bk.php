	<?php
	//$show_thumbs = FLTheme::get_setting('fl-posts-show-thumbs');
	if( get_field('manufacturer') == 'Shaw' || get_field('manufacturer') == NULL){
		
		$a=array();

		$item = get_field('sku');
		$itemImage = explode("_", $item);	
		$imageNew= $itemImage[1] .'_'. $itemImage[0];


		//Code to check the image exits.	
		$url ='https://shawfloors.scene7.com/is/image/ShawIndustries/'.$imageNew.'_MAIN?req=exists,text';
		$response = wp_remote_post($url, array('method' => 'GET'));		
		$ImageExist = $response["body"];	
		$exits = strpos($ImageExist,"catalogRecord.exists=0");		
		if ($exits === false) {	
			$a[]='https://shawfloors.scene7.com/is/image/ShawIndustries/'.$imageNew.'_MAIN?qlt=60&wid=600&hei=400&fit=crop,0';
			$image = 'https://shawfloors.scene7.com/is/image/ShawIndustries/'.$imageNew.'_MAIN?qlt=60&wid=600&hei=400&fit=crop,0';
			if(!is_singular( 'carpeting' )) {
				$url ='https://shawfloors.scene7.com/is/image/ShawIndustries/'.$imageNew.'_ROOM?req=exists,text';
				$response = wp_remote_post($url, array('method' => 'GET'));		
				$ImageExist = $response["body"];	
				$exits = strpos($ImageExist,"catalogRecord.exists=0");		
				if ($exits === false) {	
					$a[]=get_field('room_scene_image_link') . "?qlt=60&wid=600&hei=400&fit=crop,0";
				}

			}
		} else {
			$a[]='https://shawfloors.scene7.com/is/image/ShawIndustriesRender/'.$imageNew.'_MAIN?qlt=60&wid=600&hei=400&fit=crop,0';
			$image = 'https://shawfloors.scene7.com/is/image/ShawIndustriesRender/'.$imageNew.'_MAIN?qlt=60&wid=600&hei=400&fit=crop,0';
		}




		//Code to check the image exits.	
		$url ='https://shawfloors.scene7.com/is/image/ShawIndustriesRender/'.$imageNew.'_MAIN?req=exists,text';
		$response = wp_remote_post($url, array('method' => 'GET'));		
		$ImageExist = $response["body"];	
		$exits = strpos($ImageExist,"catalogRecord.exists=0");		
		if ($exits === false) {	

			$item =  array("20130107_KITCHEN_1","20140116_DININGROOM_2_SQUARE","20130408_BEDROOM_2_SQUARE","20130122_DININGROOM_1","20141218_BEDROOM_1_SQUARE");
			if(is_singular( 'carpeting' )){
				$item[]= "20150504_STAIR_RUNNER_SQUARE";
			}

			foreach ($item as $img) {
				$a[]= '//shawfloors.scene7.com/is/image/ShawIndustries/?src=ir(ShawIndustriesRender/'.$img.'?res=20&src=is(ShawIndustriesRender/'.$imageNew.'_MAIN))?qlt=60&wid=600&hei=400&fit=crop,0';
			}
		}
	}else{
		
		if(get_field('swatch_image_link') == ""){
			$https= "https://placehold.it/600x400/F2F2F2/000000?text=Coming+Soon";
		}
		else if(strpos(get_field('swatch_image_link'), "http") === false){
			$https= "https:";
		} else {
			$https = "";
		}
		$image = $a[] = 'https://mobilem.liquifire.com/mobilem?source=url[' . $https . get_field('swatch_image_link') . ']&scale=size[600x400]&sink';

		if(get_field('room_scene_image_link') == ""){
			$https= "https://placehold.it/600x400/F2F2F2/000000?text=Coming+Soon";
		}
		else if(strpos(get_field('room_scene_image_link'), "http") === false){
			$https= "https:";
		} else {
			$https = "";
		}
		$a[]=  'https://mobilem.liquifire.com/mobilem?source=url['. $https .get_field('room_scene_image_link') . ']&scale=size[600x400]&sink';

	}
	?>
	<article <?php post_class( 'fl-post' ); ?> id="fl-post-<?php the_ID(); ?>" itemscope itemtype="http://schema.org/Product">

		<header class="fl-post-header">

			<?php //FLTheme::post_top_meta(); ?>
		</header><!-- .fl-post-header -->

		<div class="fl-post-content clearfix grey-back" itemprop="text">

			<div class="container">
				<div class="row">
					<div class="col-md-8 col-sm-8 product-swatch">
						<!--                 <div class="img-responsive toggle-image" style="background-image:url(<?php the_field('swatch_image_link'); ?>?qlt=60&wid=1000&hei=550&fit=crop,0);background-size: cover;">
	<img src="<?php the_field('swatch_image_link'); ?>?qlt=60&wid=1000&hei=550&fit=crop,0" class="img-responsive toggle-image" />
	</div>
	<?php if(get_field('room_scene_image_link')){ ?>
	<div class="toggle-image-thumbnails">
	<?php 
		//                         $a=array();
		//                         $a[]=get_field('swatch_image_link').'?&scl=6&wid=400&hei=400&fit=crop';
		//                         $a[]=get_field('room_scene_image_link').'?&scl=6&wid=400&hei=400&fit=crop';

		//                         foreach($a as $k=>$v){
	?><a href="#" data-background="<?php echo $v ?>" data-fr-replace-bg=".toggle-image" style="background-image:url(<?php echo $v ?>?&scl=6&wid=128&hei=128&fit=crop)"></a><?php 
			//                          }
	?>
	</div>
	<?php } ?> 
	-->



						<div class="img-responsive toggle-image" style="background-image:url('<?php echo $image; ?>');background-size: cover;">
							<img src="<?php echo $image; ?>" class="img-responsive toggle-image" />
						</div>

						<?php

						if(get_field('room_scene_image_link')){ ?>
						<div class="toggle-image-thumbnails">
							<?php

							foreach($a as $k=>$v){
							?><a href="#" data-background="<?php echo $v ?>" data-fr-replace-bg=".toggle-image" style="background-image:url('<?php echo $v ?>');background-size: cover;"></a><?php
							}
							?>
						</div>
						<?php } ?>




					</div>
					<div class="col-md-4 col-sm-4 product-box">

						<?php get_template_part('includes/product-brand-logos'); ?>

						<?php if(get_field('parent_collection')) { ?>
						<h2><?php the_field('parent_collection'); ?> </h2>
						<?php } ?>


						<h2><?php //the_field('collection'); ?></h2>
						<h1 class="fl-post-title" itemprop="name">
							<?php the_field('style'); ?>
						</h1>
						<h2 class="fl-post-title" itemprop="name">
							<?php the_field('color'); ?>
						</h2>

						<div class="product-colors">
							<?php

							if( get_field('manufacturer') == 'COREtec' ){
								$value = get_post_meta($post->ID, 'color', true);
								$key =  "color";
							} else {

								$value = get_post_meta($post->ID, 'style', true);
								$key =  "style";
							}

							if(is_singular( 'laminate' )){
								$flooringtype = 'laminate';
							} elseif(is_singular( 'hardwood' )){
								$flooringtype = 'hardwood';
							} elseif(is_singular( 'carpeting' )){
								$flooringtype = 'carpeting';
							} elseif(is_singular( 'carpet' )){
								$flooringtype = 'carpet';
							} elseif(is_singular( 'luxury_vinyl_tile' )){
								$flooringtype = 'luxury_vinyl_tile';
							} elseif(is_singular( 'vinyl' )){
								$flooringtype = 'vinyl';
							} elseif(is_singular( 'solid_wpc_waterproof' )){
								$flooringtype = 'solid_wpc_waterproof';
							} elseif(is_singular( 'waterproof' )){
								$flooringtype = 'waterproof';
							} elseif(is_singular( 'tile' )){
								$flooringtype = 'tile';
							}

							$args = array(
								'post_type'      => $flooringtype,
								'posts_per_page' => -1,
								'post_status'    => 'publish',
								'meta_query'     => array(
									array(
										'key'     => $key,
										'value'   => $value,
										'compare' => '='
									)
								)
							);
							?>
							<?php
							$the_query = new WP_Query( $args );
							?>
							<ul>
								<li class="found"><?php  echo $the_query ->found_posts; ?></li>


								<li class="colors"><?php if($the_query ->found_posts > 1) { echo "Colors"; }else{ echo "Color";}?><br/>Available</li>
							</ul>

						</div>

						<a href="<?php echo site_url(); ?>/flooring-coupon/?keyword=<?php echo @$_COOKIE['keyword']; ?>&brand=<?php echo get_field('brand');?>" class="fl-button" role="button" style="width: auto;">
							<span class="fl-button-text">GET COUPON</span>
						</a>
						<br />
						<a href="<?php echo get_page_link(13); ?>" class="fl-button btn-white" role="button" style="width: auto;">
							<span class="fl-button-text">CONTACT US FOR INFO</span>
						</a>
						<br />
						<!-- <a href="<?php //echo get_page_link(284); ?>">FREE IN-HOME ESTIMATE ></a> -->





						<div class="product-atts">

						</div>
					</div>
				</div>


				<?php get_template_part('includes/product-color-slider'); ?>
			</div>



		</div><!-- .fl-post-content -->
		<div class="container">
			<?php get_template_part('includes/product-attributes'); ?>
		</div>

		<?php //FLTheme::post_bottom_meta(); ?>
		<?php //FLTheme::post_navigation(); ?>
		<?php //comments_template(); ?>

	</article>
	<!-- .fl-post -->