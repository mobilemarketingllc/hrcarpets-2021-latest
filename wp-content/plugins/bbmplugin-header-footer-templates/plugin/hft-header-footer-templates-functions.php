<?php

//Force templates admin on
update_option('_fl_builder_user_templates_admin','1');

//Load in the customizer settings
require_once 'hft-customizer.php';

//Shared function to Get the array of templates
function bbp_hft_get_bb_templates() {
	$data  = array();
	$query = new WP_Query( array(
		'post_type'     => 'fl-builder-template',
		'orderby'       => 'title',
		'order'       => 'ASC',
		'posts_per_page'  => '-1'
		));

	$data = array(
		'' => 'Choose A Template (Off)'
		);

	foreach( $query->posts as &$post ) {
		$data[ $post->ID ] = $post->post_title;
	}
	return $data;
}

//Set a default theme option so there aren't errors displaying
if ( get_theme_mod( 'bbp_hftcustom_header_template') == false ){
	set_theme_mod( 'bbp_hftcustom_header_template', '' );
}

if ( get_theme_mod( 'bbp_hftcustom_header_location') == false ){
	set_theme_mod( 'bbp_hftcustom_header_location', 'header_none' );
}

if ( get_theme_mod( 'bbp_hftcustom_header_absolute') == false ){
	set_theme_mod( 'bbp_hftcustom_header_absolute', 'insert_all' );
}

if ( get_theme_mod( 'bbp_hftcustom_header_opacity') == false ){
	set_theme_mod( 'bbp_hftcustom_header_opacity', 'false' );
}

if ( get_theme_mod( 'bbp_hftcustom_header_opacity_alpha') == false ){
	set_theme_mod( 'bbp_hftcustom_header_opacity_alpha', '0.0' );
}

if ( get_theme_mod( 'bbp_hftcustom_footer_template') == false ){
	set_theme_mod( 'bbp_hftcustom_footer_template', '' );
}
if ( get_theme_mod( 'bbp_hftcustom_footer_location') == false ){
	set_theme_mod( 'bbp_hftcustom_footer_location', 'footer_none' );
}

if ( get_theme_mod( 'bbp_hftcustom_content_template') == false ){
	set_theme_mod( 'bbp_hftcustom_content_template', '' );
}
if ( get_theme_mod( 'bbp_hftcustom_content_location') == false ){
	set_theme_mod( 'bbp_hftcustom_content_location', 'content_none' );
}


// Set the old settings to the new ones
$oldsetting = get_theme_mod( 'bbp_hftcustom_header_location');
if ($oldsetting == 'option0' ){
	set_theme_mod( 'bbp_hftcustom_header_location', 'header_none' );
}
else if ($oldsetting == 'option1')
{
	set_theme_mod( 'bbp_hftcustom_header_location', 'header_above_top_bar' );
}
else if ($oldsetting == 'option2')
{
	set_theme_mod( 'bbp_hftcustom_header_location', 'header_above_default_header' );
}
else if ($oldsetting == 'option3')
{
	set_theme_mod( 'bbp_hftcustom_header_location', 'header_above_content' );
}

// Set the old settings to the new ones
$oldsetting = get_theme_mod( 'bbp_hftcustom_footer_location');
if ($oldsetting == 'option0' ){
	set_theme_mod( 'bbp_hftcustom_footer_location', 'footer_none' );
}
else if ($oldsetting == 'option1')
{
	set_theme_mod( 'bbp_hftcustom_footer_location', 'footer_above_default' );
}
else if ($oldsetting == 'option2')
{
	set_theme_mod( 'bbp_hftcustom_footer_location', 'footer_below_default' );
}
else if ($oldsetting == 'option3')
{
	set_theme_mod( 'bbp_hftcustom_footer_location', 'footer_above_default' );
}
else if ($oldsetting == 'option4')
{
	set_theme_mod( 'bbp_hftcustom_footer_location', 'footer_below_default' );
}


// Set the old settings to the new ones
$oldsetting = get_theme_mod( 'bbp_hftcustom_header_absolute', 'false');

if ($oldsetting == 'true' ){
	set_theme_mod( 'bbp_hftcustom_header_absolute', 'overlay_all' );
}
else if ($oldsetting == 'homepage')
{
	set_theme_mod( 'bbp_hftcustom_header_absolute', 'overlay_homepage_insertrest' );
}
else if ($oldsetting == 'fixed_all')
{
	set_theme_mod( 'bbp_hftcustom_header_absolute', 'fixed_all' );
}
else if ($oldsetting == 'fixed_homepage')
{
	set_theme_mod( 'bbp_hftcustom_header_absolute', 'fixed_homepage_insertrest' );
}
else if ($oldsetting == 'false')
{
	set_theme_mod( 'bbp_hftcustom_header_absolute', 'insert_all' );
}


// ##############################
// CONTENT ##################
// ##############################



//If it's the customizer then hook into the previewer, otherwise just run the func as normal

global $wp_customize;
if ( isset( $wp_customize ) ) {
	add_action('customize_preview_init', 'bbp_hft_insert_custom_content_locator');
} else{
	bbp_hft_insert_custom_content_locator();
}


// Function to determine which location to insert it into based on user settings.

function bbp_hft_insert_custom_content_locator(){
	global $element_div;
	$element_div = 'div';
	$settings =  FLCustomizer::get_mods();

	if ( true == isset($settings['bbp_hftcustom_content_location'])){
		$location = $settings['bbp_hftcustom_content_location'];
	} else {
		$location = "0";
	}

	if ($location == false || $location == "0" || $location == "content_none"){

		return false;

	} elseif ($location == "fl_before_top_bar"){
		$element_div = 'div';
		$location = 'fl_before_top_bar';
		add_action( $location , 'bbp_hft_insert_custom_content_template' );

	} elseif ($location == "fl_before_header"){
		$element_div = 'div';
		$location = 'fl_before_header';
		add_action( $location , 'bbp_hft_insert_custom_content_template' );

	} elseif ($location == "fl_header_content_open"){
		$element_div = 'div';
		$location = 'fl_header_content_open';
		add_action( $location , 'bbp_hft_insert_custom_content_template' );

	} elseif ($location == "fl_header_content_close"){
		$element_div = 'div';
		$location = 'fl_header_content_close';
		add_action( $location , 'bbp_hft_insert_custom_content_template' );

	} elseif ($location == "fl_content_open"){
		$element_div = "div";
		$location = 'fl_content_open';
		add_action( $location , 'bbp_hft_insert_custom_content_template' );

	} elseif ($location == "fl_post_top_meta_open"){
		$element_div = 'div';
		$location = 'fl_post_top_meta_open';
		add_action( $location , 'bbp_hft_insert_custom_content_template' );

	} elseif ($location == "fl_post_top_meta_close"){
		$element_div = 'div';
		$location = 'fl_post_top_meta_close';
		add_action( $location , 'bbp_hft_insert_custom_content_template' );

	} elseif ($location == "fl_post_bottom_meta_open"){
		$element_div = "div";
		$location = 'fl_post_bottom_meta_open';
		add_action( $location , 'bbp_hft_insert_custom_content_template' );

	}  elseif ($location == "fl_post_bottom_meta_close"){
		$element_div = "div";
		$location = 'fl_post_bottom_meta_close';
		add_action( $location , 'bbp_hft_insert_custom_content_template' );

	}  elseif ($location == "fl_comments_open"){
		$element_div = "div";
		$location = 'fl_comments_open';
		add_action( $location , 'bbp_hft_insert_custom_content_template' );

	} elseif ($location == "fl_comments_close"){
		$element_div = 'div';
		$location = 'fl_comments_close';
		add_action( $location , 'bbp_hft_insert_custom_content_template' );

	} elseif ($location == "fl_sidebar_open"){
		$element_div = 'div';
		$location = 'fl_sidebar_open';
		add_action( $location , 'bbp_hft_insert_custom_content_template' );

	} elseif ($location == "fl_sidebar_close"){
		$element_div = "div";
		$location = 'fl_sidebar_close';
		add_action( $location , 'bbp_hft_insert_custom_content_template' );

	}  elseif ($location == "fl_after_content"){
		$element_div = "div";
		$location = 'fl_after_content';
		add_action( $location , 'bbp_hft_insert_custom_content_template' );

	}  elseif ($location == "fl_before_footer_widgets"){
		$element_div = "div";
		$location = 'fl_before_footer_widgets';
		add_action( $location , 'bbp_hft_insert_custom_content_template' );

	}  elseif ($location == "fl_after_footer_widgets"){
		$element_div = "div";
		$location = 'fl_after_footer_widgets';
		add_action( $location , 'bbp_hft_insert_custom_content_template' );

	} elseif ($location == "fl_before_footer"){
		$element_div = 'div';
		$location = 'fl_before_footer';
		add_action( $location , 'bbp_hft_insert_custom_content_template' );

	} elseif ($location == "fl_after_footer"){
		$element_div = 'div';
		$location = 'fl_after_footer';
		add_action( $location , 'bbp_hft_insert_custom_content_template' );

	}  elseif ($location == "fl_page_close"){
		$element_div = "div";
		$location = 'fl_page_close';
		add_action( $location , 'bbp_hft_insert_custom_content_template' );
	}

}


// This function checks the page is suitable and then inserts it into where specified.

function bbp_hft_insert_custom_content_template() {

	if (get_page_template_slug() !== 'tpl-no-header-footer.php' ){

		$settings =  FLCustomizer::get_mods();
		$template = $settings['bbp_hftcustom_content_template'];
		global $element_div;

		if ($template !== '' ){
			echo '<'.$element_div.' id="djcustom-content" >' . do_shortcode('[fl_builder_insert_layout id="'.$template.'"]') . '</' . $element_div . '>';
		}
	}
}







// ##############################
// HEADER ##################
// ##############################

//If it's the customizer then hook into the previewer, otherwise just run the func as normal

global $wp_customize;
if ( isset( $wp_customize ) ) {
	add_action('customize_preview_init', 'bbp_hft_insert_custom_header_locator');
	add_action('customize_preview_init', 'bbp_hft_custom_header_styling');

} else{
	bbp_hft_insert_custom_header_locator();
	bbp_hft_custom_header_styling();
}



// Function to determine which location to insert it into based on user settings.

function bbp_hft_insert_custom_header_locator(){
	global $element_header;
	$element_header = 'header';
	$settings =  FLCustomizer::get_mods();

	if ( true == isset($settings['bbp_hftcustom_header_location'])){
		$location = $settings['bbp_hftcustom_header_location'];
	} else {
		$location = "0";
	}

	if ($location == false OR $location == "0"){

		return false;

	} elseif ($location == "header_none"){

		return false;

	} elseif ($location == "header_above_top_bar"){
		$element_header = "header";
		$location = 'fl_before_top_bar';
		add_action( $location , 'bbp_hft_insert_custom_header_template' );

	} elseif ($location == "header_above_default_header"){
		$element_header = "header";
		$location = 'fl_before_header';
		add_action( $location , 'bbp_hft_insert_custom_header_template' );

	} elseif ($location == "header_above_content"){
		$element_header = "header";
		$location = 'fl_before_content';
		add_action( $location , 'bbp_hft_insert_custom_header_template' );

	}

}


// This function checks the page is suitable and then inserts it into where specified.

function bbp_hft_insert_custom_header_template() {

	if (get_page_template_slug() !== 'tpl-no-header-footer.php' )
	{
		$settings =  FLCustomizer::get_mods();
		if ($settings['bbp_hftcustom_header_absolute'] == 'insert_all' ||
			$settings['bbp_hftcustom_header_absolute'] == 'overlay_all' ||
			$settings['bbp_hftcustom_header_absolute'] == 'fixed_all' ||
			$settings['bbp_hftcustom_header_absolute'] == 'overlay_homepage_insertrest' ||
			$settings['bbp_hftcustom_header_absolute'] == 'fixed_homepage_insertrest')
		{
			global $element_header;
			$template = $settings['bbp_hftcustom_header_template'];
			if ($template !== '' )
			{
				echo '<'. $element_header .' id="djcustom-header" itemscope="itemscope" itemtype="http://schema.org/WPHeader">' . do_shortcode('[fl_builder_insert_layout id="'.$template.'"]') . '</'. $element_header . '>';
			}

		}
		else if( is_front_page())
		{
			global $element_header;
			$template = $settings['bbp_hftcustom_header_template'];
			if ($template !== '' )
			{
				echo '<'. $element_header .' id="djcustom-header" itemscope="itemscope" itemtype="http://schema.org/WPHeader">' . do_shortcode('[fl_builder_insert_layout id="'.$template.'"]') . '</'. $element_header . '>';
			}
		}

	}
}


function bbp_hft_custom_header_styling()
{
	$settings =  FLCustomizer::get_mods();

	// if ( $settings['bbp_hftcustom_header_absolute'] == 'overlay_homepage' ||
	// 	$settings['bbp_hftcustom_header_absolute'] == 'overlay_all' ||
	// 	$settings['bbp_hftcustom_header_absolute'] == 'fixed_homepage' ||
	// 	$settings['bbp_hftcustom_header_absolute'] == 'fixed_all')
	// {
	// }
	add_action( 'wp_head', 'bb_hft_header_styling');



	if ( $settings['bbp_hftcustom_header_opacity'] !== 'false')
	{
		add_action( 'wp_head', 'bb_hft_header_opacity_styling');
	}

}



function bb_hft_header_styling()
{
	$settings =  FLCustomizer::get_mods();

	$inserthomespacer = '';
	if ($settings['bbp_hftcustom_header_absolute'] == 'overlay_homepage' ||
		$settings['bbp_hftcustom_header_absolute'] == 'fixed_homepage' ||
		$settings['bbp_hftcustom_header_absolute'] == 'insert_homepage' ||
		$settings['bbp_hftcustom_header_absolute'] == 'overlay_homepage_insertrest' ||
		$settings['bbp_hftcustom_header_absolute'] == 'fixed_homepage_insertrest')
	{
		$inserthomespacer = '.home';
	}


	$csspositiontype = '';
	if ($settings['bbp_hftcustom_header_absolute'] == 'fixed_homepage' ||
		$settings['bbp_hftcustom_header_absolute'] == 'fixed_all' ||
		$settings['bbp_hftcustom_header_absolute'] == 'fixed_homepage_insertrest')
	{
		$csspositiontype = 'fixed';

	}
	else if ($settings['bbp_hftcustom_header_absolute'] == 'overlay_homepage' ||
		$settings['bbp_hftcustom_header_absolute'] == 'overlay_all' ||
		$settings['bbp_hftcustom_header_absolute'] == 'overlay_homepage_insertrest')
	{
		$csspositiontype = 'absolute';
	}
	else if ($settings['bbp_hftcustom_header_absolute'] == 'insert_homepage' ||
		$settings['bbp_hftcustom_header_absolute'] == 'insert_all')
	{
		$csspositiontype = 'relative';
	}

	if ( is_archive() || is_author() || is_category() || is_tag() || 'post' == get_post_type())
	{
		$csspositiontype = 'relative';
	}



	$admin_bb_bars_css = '';
	if ($settings['bbp_hftcustom_header_absolute'] == 'fixed_homepage' ||
		$settings['bbp_hftcustom_header_absolute'] == 'fixed_all' ||
		$settings['bbp_hftcustom_header_absolute'] == 'fixed_homepage_insertrest')
	{
		//set the adminbar
		$admin_bb_bars_css .= $inserthomespacer.'.admin-bar #djcustom-header{
			top: 32px;
			z-index: 99999;}';

		$admin_bb_bars_css .= '.fl-builder-edit '.$inserthomespacer.' #djcustom-header{
			top: 43px;}';
	}


	//override for bbuilder editor
	$admin_bb_bars_css .= '.fl-builder-edit #djcustom-header{
		z-index: 100007;}';
//override for bbuilder
		$admin_bb_bars_css .= '.fl-builder-edit #djcustom-header:hover:before{
			content: "Click Here to Disable Header Overlay Effects In Editor";
			font-size: 12px;
			position: absolute;
			text-decoration: underline;
			z-index: 10;
			pointer-events: none;
		}';

//override changes in editor
		echo "<script>
		jQuery(document).ready(function(){
			jQuery('#djcustom-header').click(function(){
				jQuery('.fl-builder-edit #djcustom-header').css('position','relative').css('top','0').css('z-index','0');
			});
		});
	</script>";





	echo '<style type="text/css">
			#djcustom-header{
	position: relative;
	width: 100%;
	top: 0;
	left: 0;
	z-index: 30;
}
'.$inserthomespacer.' #djcustom-header
{
	position: '.$csspositiontype.';
}
'.$admin_bb_bars_css.'
</style>';





}


function bb_hft_header_opacity_styling()
{
	$settings =  FLCustomizer::get_mods();

	if ( is_archive() || is_author() || is_category() || is_tag() || 'post' == get_post_type())
	{
		return;
	}

	if ($settings['bbp_hftcustom_header_opacity'] == 'homepage')
	{
		echo '<script>
		jQuery(document).ready(function(){

			var colour = jQuery(".home #djcustom-header .fl-row-content-wrap").css("backgroundColor");

			if (typeof colour != "undefined")
			{
				colour = rgbtorgba(colour);
			}

			if (typeof colour != "undefined")
			{
				var new_rgba_str = "rgba(" + colour.substring(colour.lastIndexOf("(")+1,colour.lastIndexOf(",")) + ", '.$settings["bbp_hftcustom_header_opacity_alpha"].')";
				jQuery(".home #djcustom-header .fl-row-content-wrap").css("backgroundColor",new_rgba_str);
			}

			function rgbtorgba(bg){
				if(bg.indexOf("a") == -1)
				{
					var result = bg.replace(")", ", '.$settings["bbp_hftcustom_header_opacity_alpha"].')").replace("rgb", "rgba");
					return result;
				}
				else
				{
					return bg;
				}
			}
		});
	</script>';

}
else //all pages
{

	echo '<script>
	jQuery(document).ready(function(){

		var colour = jQuery("#djcustom-header .fl-row-content-wrap").css("backgroundColor");

		if (typeof colour != "undefined")
		{
			colour = rgbtorgba(colour);
		}

		if (typeof colour != "undefined")
		{
			var new_rgba_str = "rgba(" + colour.substring(colour.lastIndexOf("(")+1,colour.lastIndexOf(",")) + ", '.$settings["bbp_hftcustom_header_opacity_alpha"].')";
			jQuery("#djcustom-header .fl-row-content-wrap").css("backgroundColor",new_rgba_str);
		}

		function rgbtorgba(bg){
			if(bg.indexOf("a") == -1)
			{
				var result = bg.replace(")", ", '.$settings["bbp_hftcustom_header_opacity_alpha"].')").replace("rgb", "rgba");
				return result;
			}
			else
			{
				return bg;
			}
		}
	});
</script>';
}

}











// ##############################
// FOOTER ##################
// ##############################



//If it's the customizer then hook into the previewer, otherwise just run the func as normal

global $wp_customize;
if ( isset( $wp_customize ) ) {
	add_action('customize_preview_init', 'bbp_hft_insert_custom_footer_locator');
} else{
	bbp_hft_insert_custom_footer_locator();
}


// Function to determine which location to insert it into based on user settings.

function bbp_hft_insert_custom_footer_locator(){
	global $element_footer;
	$element_footer = 'footer';
	$settings =  FLCustomizer::get_mods();

	if ( true == isset($settings['bbp_hftcustom_footer_location'])){
		$location = $settings['bbp_hftcustom_footer_location'];
	} else {
		$location = "0";
	}

	if ($location == false OR $location == "0" || $location == "footer_none"){

		return false;

	} elseif ($location == "footer_above_default"){
		$element_footer = 'footer';
		$location = 'fl_after_content';
		add_action( $location , 'bbp_hft_insert_custom_footer_template' );

	} elseif ($location == "footer_below_default"){
		$element_footer = 'footer';
		$location = 'fl_page_close';
		add_action( $location , 'bbp_hft_insert_custom_footer_template' );

	}
}


// This function checks the page is suitable and then inserts it into where specified.

function bbp_hft_insert_custom_footer_template() {

	if (get_page_template_slug() !== 'tpl-no-header-footer.php' ){

		$settings =  FLCustomizer::get_mods();
		$template = $settings['bbp_hftcustom_footer_template'];
		global $element_footer;

		if ($template !== '' ){
			echo '<'.$element_footer.' id="djcustom-footer" itemscope="itemscope" itemtype="http://schema.org/WPFooter" >' . do_shortcode('[fl_builder_insert_layout id="'.$template.'"]') . '</' . $element_footer . '>';
		}
	}
}














